import 'package:flutter/material.dart';
import 'package:wuzzef/Localization/all_translations.dart';
import 'package:wuzzef/Screen/Settings/ContactUs.dart';
import 'package:wuzzef/Screen/Settings/Langauge.dart';
import 'package:wuzzef/Services/Colors.dart';

class Settings extends StatefulWidget {
  final bool isLogin;

  const Settings({Key key, this.isLogin}) : super(key: key);
  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          allTranslations.text("Settings"),
          style: TextStyle(color: primaryColor),
        ),
      ),
      body: ListView(
        children: <Widget>[
          InkWell(
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => LanguageScreen(true)));
            },
            child: Padding(
              padding: const EdgeInsets.all(24),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Row(
                    children: <Widget>[
                      Icon(
                        Icons.language,
                        color: primaryColor,
                      ),
                      SizedBox(
                        width: 8,
                      ),
                      Text(allTranslations.text("Language"))
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      // Consumer<LanguageProvider>(builder: (context, model, _) {
                      //   return Text(model.locale.languageCode);
                      // }),
                      SizedBox(
                        width: 8,
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        color: primaryColor,
                      )
                    ],
                  )
                ],
              ),
            ),
          ),
          _item(
              title: allTranslations.text("Password"),
              icon: Icons.lock,
              onTap: () {}),
          _item(
              title: allTranslations.text("Mobile Number"),
              icon: Icons.phone,
              onTap: () {}),
          _item(
              title: allTranslations.text("Contact Us"),
              icon: Icons.file_upload,
              onTap: () {}),
          _item(
              title: allTranslations.text("Professional"),
              icon: Icons.card_giftcard,
              onTap: () {}),
          _item(
              title: allTranslations.text("FAQ"),
              icon: Icons.help,
              onTap: () {}),
          _item(
              title: allTranslations.text("Contact Us"),
              icon: Icons.contact_mail,
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ContactUs()));
              }),
        ],
      ),
    );
  }

  Padding _item(
      {@required String title,
      @required IconData icon,
      @required Function onTap}) {
    return Padding(
      padding: const EdgeInsets.all(24),
      child: InkWell(
        onTap: () {
          onTap();
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Row(
                children: <Widget>[
                  Icon(
                    icon,
                    color: primaryColor,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Expanded(
                    child: Text(
                      title,
                      overflow: TextOverflow.ellipsis,
                    ),
                  )
                ],
              ),
            ),
            Icon(
              Icons.arrow_forward_ios,
              color: primaryColor,
            )
          ],
        ),
      ),
    );
  }
}
