
import 'package:flutter/material.dart';
import 'package:http/http.dart'as http;
import 'package:wuzzef/Localization/all_translations.dart';
import 'package:wuzzef/Services/Colors.dart';
class ContactUs extends StatefulWidget {
  @override
  _ContactUsState createState() => _ContactUsState();
}

class _ContactUsState extends State<ContactUs> {
  final _formKey = GlobalKey<FormState>();

  final TextEditingController _controller = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
         allTranslations.text( "Contact Us"),
          style: TextStyle(color: primaryColor),
        ),
      ),
      body: Center(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Container(
                  width: 180, height: 180,
                    child: Image.asset("lib/assets/images/whatsapp.jpg")),
              ),
              Padding(
                padding: const EdgeInsets.all(16.0),
                child: Form(
                  key: _formKey,
                  child: Card(
                    color: Colors.white,
                    shape: RoundedRectangleBorder(
                      side:
                          BorderSide(color: primaryColor, width: 1.0),
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: TextFormField(
                        controller: _controller,validator: (value){
                          return value==null||value==""?allTranslations.text("put valid input"):null;
                        },
                        maxLines: 10,
                        decoration: InputDecoration(
                          hintStyle: TextStyle(
                            color: Colors.grey,
                            fontFamily: "Roboto",
                            fontWeight: FontWeight.w300,
                          ),
                          border: InputBorder.none,
                          hintText:allTranslations.text( "Send your Message …."),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              ButtonTheme(
                minWidth: MediaQuery.of(context).size.width / 1.2,
                height: 40,
                child: OutlineButton(
                  color: primaryColor,
                  child: Text(
                 allTranslations.text(   "Send Meesage"),
                    style: TextStyle(color: primaryColor),
                  ),
                  shape: new RoundedRectangleBorder(
                      borderRadius: new BorderRadius.circular(5.0)),
                  borderSide: BorderSide(color: primaryColor),
                  onPressed: () async{
                    if(!_formKey.currentState.validate()){

                      return;
                    }
                await http.post("");
                
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
