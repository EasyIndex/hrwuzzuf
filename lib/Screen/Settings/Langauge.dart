import 'package:flutter/material.dart';
import 'package:wuzzef/Localization/all_translations.dart';
import 'package:wuzzef/Services/Colors.dart';
import 'package:wuzzef/Services/Providers/LanguageProvider.dart';

class LanguageScreen extends StatelessWidget {
  LanguageScreen(this.isPop);
  final bool isPop;
  final List<_Data> languages = [
    _Data(
      "عربي",
      "assets/images/flags/saudi_arabia.png",
    ),
    _Data("English", "assets/images/flags/Group_5501.png"),
    _Data("Française", "assets/images/flags/Group_5502.png"),
    _Data("русский", "assets/images/flags/Group_5500.png"),
    _Data("Türk", "assets/images/flags/Group_5499.png"),
    _Data("italiana", "assets/images/flags/Group_5498.png"),
    _Data("Deutsch", "assets/images/flags/Group_5497.png"),
    _Data("中文", "assets/images/flags/Group_5496.png")
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: isPop ? AppBar() : null,
      body: ListView(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(24.0),
            child: Center(
              child: Text(
                  isPop
                      ? allTranslations.text("Choose Langauge")
                      : "Choose Langauge",
                  style: TextStyle(color: primaryColor, fontSize: 20)),
            ),
          ),
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemCount: languages.length,
            itemBuilder: (BuildContext context, int index) {
              return InkWell(
                onTap: () async {
                  //  await loadWidget(context, setlangLocalyAndtoServer(index, langauge));
                  //     if (isPop) {
                  //       Navigator.pop(context);
                  //     } else {
                  //       Navigator.pushReplacement(
                  //           context,
                  //           MaterialPageRoute(
                  //               builder: (context) => IntroductionPage()));
                  //     }
                },
                child: Container(
                  height: 50,
                  margin: EdgeInsets.symmetric(
                      horizontal: MediaQuery.of(context).size.width * .1,
                      vertical: 16),
                  decoration: ShapeDecoration(
                      color: Colors.white,
                      shadows: [
                        BoxShadow(
                            offset: Offset(0, 3),
                            blurRadius: 6,
                            color: Color(0xff4d000000))
                      ],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10))),
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(children: <Widget>[
                      Image.asset(
                        languages.elementAt(index).imagePath,
                        width: 40,
                        height: 40,
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        languages.elementAt(index).title,
                        style: TextStyle(fontSize: 18),
                      )
                    ]),
                  ),
                ),
              );
            },
          ),
        ],
      ),
    );
  }

  Future setlangLocalyAndtoServer(int index, LanguageProvider langauge) async {
    //   await   AuthProvider().setLanguage(languages[index].title);
    //  await   langauge.setLang(languages[index].title);
  }
}

class _Data {
  String title;
  String imagePath;
  _Data(this.title, this.imagePath);
}
