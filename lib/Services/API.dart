import 'package:dio/dio.dart';

class API {
  Dio _dio = Dio();
  final String _baseUrl = "www";
  Future getData() async {
    await _dio.get(_baseUrl);
  }
}
