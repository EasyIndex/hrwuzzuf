import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:wuzzef/Localization/all_translations.dart';
import 'package:wuzzef/Screen/Settings/Settings.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  allTranslations.init(Locale("en"));
  runApp(MyApp());
}
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      locale: Locale("en"),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: allTranslations.supportedLocales(),
      theme: ThemeData(
          canvasColor: Colors.white,
          appBarTheme: AppBarTheme(
            color: Colors.white,
            elevation: 0,
          )),
      home: Settings(),
    );
  }
}
