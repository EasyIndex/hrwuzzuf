import 'dart:async';
import 'dart:convert';
import 'dart:ui';
import 'package:flutter/services.dart';

const List<String> _supportedLanguages = [
  'en',
  'fr',
  'ar',
  'ru',
  'tr',
  'it',
  'de',
  'zh'
];

class GlobalTranslations {
  Locale _locale = Locale("en");
  Map<dynamic, dynamic> _localizedValues;
  VoidCallback _onLocaleChangedCallback;

  init(Locale locale) {
    _locale = locale;
    setNewLanguage("en");
  }

  /// Returns the list of supported Locales

  Iterable<Locale> supportedLocales() =>
      _supportedLanguages.map<Locale>((lang) => new Locale(lang,));


  String text(String key) {
    // Return the requested string
    print(locale.languageCode);
    print(_localizedValues);
    return (_localizedValues == null || _localizedValues[key] == null)
        ? '** $key not found'
        : _localizedValues[key];
  }

  get currentLanguage => _locale == null ? '' : _locale.languageCode;

  Locale get locale => _locale;

  Future<Null> setNewLanguage(
      [String newLanguage, bool saveInPrefs = false]) async {
    String language = newLanguage;

    // Set the locale
    if (language == "") {
      language = "en";
    }
    _locale = Locale(
      language,
    );

    // Load the language strings
    print("assets/locale/localization_${locale.languageCode}.json");
    print("aaaaaa");
    String jsonContent = await rootBundle
        .loadString("assets/locale/localization_${locale.languageCode}.json");
    _localizedValues = json.decode(jsonContent);

    // If there is a callback to invoke to notify that a language has changed
    if (_onLocaleChangedCallback != null) {
      _onLocaleChangedCallback();
    }

    return null;
  }

  set onLocaleChangedCallback(VoidCallback callback) {
    _onLocaleChangedCallback = callback;
  }

  static final GlobalTranslations _translations =
      new GlobalTranslations._internal();
  factory GlobalTranslations() {
    return _translations;
  }
  GlobalTranslations._internal();
}

GlobalTranslations allTranslations = new GlobalTranslations();
